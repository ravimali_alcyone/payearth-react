import { configureStore } from "@reduxjs/toolkit";
import authSlice from './reducers/auth-reducer';
import catSearchSlice from './reducers/cat-search-reducer';
import globalSlice from "./reducers/global-reducer";
import productSlice from './reducers/product-reducer';
import serviceSlice from './reducers/service-reducer';
import orderSlice from './reducers/order-reducer';
import wishlistSlice from './reducers/wishlist-reducer';
import postSlice from './reducers/post-reducer'

export default configureStore({
    reducer: {
        auth: authSlice,
        catSearch: catSearchSlice,
        product: productSlice,
        service: serviceSlice,
        global: globalSlice,
        order: orderSlice,
        wishlist: wishlistSlice,
        post: postSlice
    }
})